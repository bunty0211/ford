using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class CarController : Controller
    {

        public IActionResult Sedan()
        {
            return View();
        }
        public IActionResult Suv()
        {
            return View();
        }
        public IActionResult Hatchback()
        {
            return Content("<html><body><h1>Hatchback</h1></body></html>", "text/html");
        }
        public IActionResult Description()
        {
            return Content("This is about Car Controller.");
        }
        public IActionResult Vehicle() {
            return Json(new{ name = "Ford", description = "Company of the car" });
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
