using System;
using Xunit;

namespace UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var expected = true;
            int x = 151;
            var actual = palindrome(x);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(130, false)]
        public void Test2(int x, bool expected)
        {
            Assert.Equal(expected, palindrome(x));
        }

        bool palindrome(int x){
            var result = 0;
            var temp = x;
            while(temp>0){
                result = result*10 + temp%10;
                temp = temp/10;
            }
            return result == x;
        }
    }
}
